function p=gentau(varargin)
% GENTAU: to be called after q(c)swibra to generate a linear combination of 
% the tangents corresponding to zero eigenvalues stored in p.mat.ker 
% The new tangent is stored in p.tau.
% default call: p=gentau(p,v), 
% with v=coefficients of tangents stored in p.mat.ker, 
% normalized wrt xi-norm, store in p.tau, plot in figure 6
% 
% alternatives: p=gentau(p,v,'ndir'), p=gentau(p,v,'ndir',lamdot)
%
lamdot=0; fname=[]; 
if isstruct(varargin{1}); p=varargin{1}; v=varargin{2}; 
 if nargin>2; fname=varargin{3}; end; 
 if nargin>3; lamdot=varargin{4}; end ; 
end
if ~isempty(fname); p=setfn(p,fname); end 
p.file.count=0; p.fuha.savefu(p); p.file.count=1;
t=0; ker=p.mat.ker; 
for i=1:size(v(:),1); t=t+ker(:,i)*v(i); end
t=t/xinorm(t,p.sol.xi,0,0); p.tau=t;
plotsolu(p,[p.tau; p.u(p.nu+1:end)],6,p.plot.pcmp,p.plot.pstyle); 
if lamdot~=0; p.tau(end)=lamdot; end 