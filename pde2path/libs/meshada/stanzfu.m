function z=stanzfu(p)
% stanzfu: choose function z for trullekrul from data in p 
np=p.np; u=p.u(1:np); z=u; 
