classdef spherepdeo < pde  
% spherepdeo: (preimage-)domain and mesh for a sphere 
% modification of stanpdeo2D to have coarser grids near poles  
% sw.ref: #refinement steps, sw.sym: if 1, then use sym.mesh 
methods(Access = public)
  function o=spherepdeo(lx,ly,n1,n2,ref,yexp)
     d1=2*lx/n1; 
     t1x=linspace(-lx+d1,lx-d1,n1);  t1y=ly*ones(1,n1); % south 
     t=linspace(-1,1,n2); t2=t.^3; 
     t2x=lx*ones(1,n2); t2y=ly*(1-t2)/2; % figure(1); clf; plot(t2y), pause 
     t3x=-t1x; t3y=(pi-ly)*ones(1,n1);
     t4x=-lx*ones(1,n2); t4y=ly*(1+t2)/2; %figure(1); clf; plot(t4y), pause
     t1=linspace(-1,1,n2); t1=sign(t1).*abs(t1).^yexp; 
     x=[]; y=[]; P=[]; 
     for i=1:n2
         nx=n2+n1-round(abs(n2*t1(i))); 
         xa=linspace(-lx,lx,nx); ya=ly*t1(i)*ones(1,nx); 
         x=[x xa]; y=[y ya]; P=[P [xa; ya]]; 
     end
     P=P'; P=unique(P,'rows','stable'); P=P'; 
     dt=delaunayTriangulation(P'); po=dt.Points'; e=dt.freeBoundary'; t=dt.ConnectivityList';        
     t=[t; ones(1,size(t,2))]; 
     se=size(e,2); ed=[e; zeros(3,se)]; ed(5,:)=ones(1,se);
     ed(4,:)=ones(1,se); ed(3,:)=ones(1,se); 
     grid=grid2DHU; grid.p=po; grid.t=t; grid.e=ed; 
     o.grid=grid; o.fem=lagrange12D;
   %  [x2,i2]=o.grid.bdseg(1); 
   %  if ref>0; pause; end
     for i=1:ref; o.grid.refineMesh; end;
   %  [x2,i2]=o.grid.bdseg(1); 
   end
end
methods(Access = protected) % only here since required by pde-class
    function r=df(~,~,~); r=0; end % rather use p.fuha.sG in pderesi
end
end
