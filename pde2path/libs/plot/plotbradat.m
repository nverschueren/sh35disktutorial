function plotbradat(p,wnr,xc,yc)
% PLOTBRADAT: plot branch component vs. branch component
%
%  plotbradat(p,wnr,xc,yc)
% xc=x-axis component
% yc=y-axis component
%
% See also plotbra, bradat, stanbra
figure(wnr); plot(p.branch(xc,:),p.branch(yc,:),'*-');
