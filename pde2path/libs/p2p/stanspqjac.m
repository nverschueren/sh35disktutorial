function Qvvph=stanspqjac(p,u) % here zero, i.e., linear q-eqn!
Qvvph=sparse(p.nc.nq,p.nu);
end 