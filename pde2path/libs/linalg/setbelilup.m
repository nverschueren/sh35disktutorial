function p=setbelilup(p,bw,beltol,belimax,dtol,maxit)
% setbelilup:convenience function for switching on bel with AMGsolver 
% p=setbelilup(p,bw,beltol,belimax,dtol,maxit)
p.fuha.lss=@lssbel; p.fuha.blss=@blssbel; % choosing bordered elim. LSS 
p.bel.bw=bw; p.bel.tol=beltol; p.bel.imax=belimax; % param. for bordered elim. LSS 
p.fuha.innerlss=@lssAMG; p.ilup.droptol=dtol; p.ilup.droptolS=dtol/10;
p.ilup.maxit=maxit; 