%%
% script SETPDEPATH: set the path for pde2path (and possibly ilupack)
% 
% to set path to extra-libs, set extra=1 below, and set extralibs accordingly 
% This is a simplified version of setpde2path, tailored for the SH35 on the
% disk tutorial.
global pphome
pphome=pwd; 
fprintf('%s\n',['pde2path, v2.9. Setting library path beginning with ' pphome]); 
addpath(genpath([pphome,'/libs']));
addpath([pphome,'/OOPDElightNA']);
addpath(genpath([pphome,'/html']));  % add html with subfolders 


extra=0;% set extra=1 and adapt paths for ilupack and other stuff
if extra; cd ..; ppup=''; 
extralibs={[ppup,'/ilupackV2.4_GNU64_long_MUMPS/mex'],[ppup,'/myp2plib']}; 
%extralibs={[ppup,'/ilupack4v2020b/mex'],[ppup,'/myp2plib']}; 
  for i=1:length(extralibs)
  fprintf(['Setting additional libraries ' extralibs{i} '\n']);  
  lastwarn(''); addpath(extralibs{i}); s=lastwarn; 
  if strncmp(lastwarn,'Name is nonexistent',15); 
   fprintf(' *** wrong path for extralibs in setpde2path, please check ***\n'); 
   cd(pphome); 
   edit setpde2path.m; 
  end 
  cd(pphome); 
  end
else fprintf('*** No additional libs put into path ***\n'); 
end
try [prec,opt]=AMGfactor(sparse(1),[]);  
catch fprintf('No ilupack found: If you want it, edit setpde2path.m \n'); 
end 
